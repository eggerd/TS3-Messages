<?php

/**
 * This script checks the database of a TS3 server for unread offline messages and notifies the
 * receivers of those unread messages via poke.
 */

require_once('./vendor/autoload.php');
require_once('./configs/configure.php');
require_once('./includes/bugsnag.php');
require_once('./includes/functions/helper.php');
require_once('./includes/functions/data.php');
require_once('./includes/classes/client.php');
// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet

$DB = new mysqli(MYSQL_HOSTNAME, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE);
if($DB->connect_error)
{
	trigger_error($DB->connect_error, E_USER_NOTICE);
	throw new Exception('Failed to establish connection to the TS3 servers database!');
}

$TS3QUERY = new ts3admin(TS3_HOSTNAME, TS3_QUERY_PORT);
$ts3query_connect = $TS3QUERY->connect();
if(!$ts3query_connect['success'])
{
	trigger_query_error($ts3query_connect['errors']);
	throw new Exception('Failed to establish connection to the TS3 query!');
}

$ts3query_login = $TS3QUERY->login(TS3_QUERY_USERNAME, TS3_QUERY_PASSWORD);
if(!$ts3query_login['success'])
{
	trigger_query_error($ts3query_login['errors']);
	throw new Exception('Failed to authenticate query account!');
}

$ts3query_select = $TS3QUERY->selectServer(TS3_SERVER_PORT);
if(!$ts3query_select['success'])
{
	trigger_query_error($ts3query_select['errors']);
	throw new Exception('Failed to select virtual server!');
}

if($TS3QUERY->whoAmI()['data']['client_nickname'] !== TS3_NICKNAME)
{
	$ts3query_rename = $TS3QUERY->setName(TS3_NICKNAME);
	if(!$ts3query_rename['success'])
	{
		trigger_query_error($ts3query_rename['errors']);
		throw new Exception('Failed to rename query user!');
	}
}

$ts3query_clientlist = $TS3QUERY->clientList('-country');
if(!$ts3query_clientlist['success'])
{
	trigger_query_error($ts3query_clientlist['errors']);
	throw new Exception('Failed to get list of currently connected clients!');
}


// +++++ Checking Offline Messages +++++

$clients = array();

// create a client object for each user that is currently online
foreach($ts3query_clientlist['data'] as $client)
{
	if($client['client_type'] == "0") // only if not a query user
	{
		$clients[$client['client_database_id']] = new client($client['clid'], $client['client_database_id'], $client['client_country']);
	}
}

// get all unread messages for every user currently online (at once)
$fetch = client::fetch_unread_messages($DB, $clients);

// import data from the memory file
client::import($clients, data_read());

// notify all clients that have unread messages & haven't received a notification in recent time
foreach($clients as $client)
{
	$poke = $client->notify($TS3QUERY);
	if(!$poke['success'])
	{
		trigger_query_error(['Failed to notify user with database ID #'.$client->database_id], $poke['errors']);
	}
}

// export data into file for evaluation during next execution
$export = data_write(client::export($clients));

?>
