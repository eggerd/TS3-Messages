<?php

config::set('VERSION', '2.1.1');


// ***** Teamspeak Query *****

// The FQDN or IP of the TS3 server
config::set('TS3_HOSTNAME', '127.0.0.1'); // :string

// The port number of the TS3 server itself
config::set('TS3_SERVER_PORT', 9987); // :integer

// The port number of the TS3 query
config::set('TS3_QUERY_PORT', 30033); // :integer

// The username of the query account (see documentation for a list of required permissions)
config::set(config::secret('TS3_QUERY_USERNAME')); // :string

// The password for the query account
config::set(config::secret('TS3_QUERY_PASSWORD')); // :string

// The username that should be used when connecting to the TS3 query & poking users
config::set('TS3_NICKNAME', 'Server [Messages]'); // :string



// ***** Teamspeak Database *****

// The FQDN or IP of the TS3 servers database
config::set('MYSQL_HOSTNAME', 'localhost'); // :string

// The username of the database account (see documentation for a list of required permissions)
config::set(config::secret('MYSQL_USERNAME')); // :string

// The password for the database account
config::set(config::secret('MYSQL_PASSWORD')); // :string

// The name of the database that is used by the TS3 server
config::set('MYSQL_DATABASE', 'ts3_server'); // :string



// ***** General *****

// The name for the data file that will contain information about the last time a user has received
// a notification & for what messages
config::set('DATA_FILENAME', 'evaluated.json'); // :string

// Time in seconds before a user receives a new notification for the same message(s)
config::set('NOTIFICATION_POKE_TIMEOUT_DURATION', 3570); // :integer

// Two letter country code that exists in the LANGUAGES array (case sensitive!) and should be used
// as the default language, in case a users language could not be determined or isn't available
config::set('DEFAULT_LANGUAGE', 'EN'); // :string

// The following array contains all messages used fore poke notifications in different languages.
// Format: {'TWO_LETTER_COUNTRY_CODE' => {'SINGULAR_MESSAGE', 'PLURAL_MESSAGE', 'SHORTCUT_INFO'}}
// Description:
//    - TWO_LETTER_COUNTRY_CODE = the two letter country code (in all caps) used by TS3 in the "client_country" field (e.g. "EN" for English)
//    - SINGULAR_MESSAGE = the poke messages for a single unread message
//    - PLURAL_MESSAGE = the poke message for multiple unread messages
//    - SHORTCUT_INFO = should be the full navigation path for opening offline messages, as well as the keyboard shortcut
// Use "%n" (without the quotation marks) inside of SINGULAR_MESSAGE & PLURAL_MESSAGE as placeholder
// for the amount of unread messages
config::set('LANGUAGES', [
  'EN' => ['You have %n unread message!', 'You have %n unread messages!', '[Tools -> Offline Messages] or [Ctrl + O]'],
  'DE' => ['Du hast %n ungelesene Nachricht!', 'Du hast %n ungelesene Nachrichten!', '[Extras -> Offline Nachrichten] oder [Strg + O]']
]);



/***** Error Tracking *****/

// API key for the Bugsnag project (leave empty to disable tracking)
config::set('BUGSNAG_API_KEY', ''); // :string

// List of environment names that will be tracked by Bugsnag
config::set('BUGSNAG_TRACK_ENVIRONMENTS', ['prod', 'test']); // :array


?>
