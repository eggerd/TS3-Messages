<?php

// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet

/**
 * Represents a user of the Teamspeak 3 server
 */
class client
{
	/**
	 * @var integer|boolean $session_id = clid of the user, or FALSE if the user isn't currently online
	 * @var integer $database_id = the unique database ID of this user
	 * @var string $language = two letter country code of the users language
	 * @var integer $notification = a UNIX timestamp of the last time this user received a notification
	 * @var array $messages = list of IDs of unread messages of this user
	 */
	public $session_id, $database_id, $language, $notification_time, $messages = array();



	/**
	 * Constructor
	 *
	 * @param integer|boolean $session_id = clid of the user, or FALSE if the user isn't currently online
	 * @param integer $database_id = the unique database ID of this user
	 * @param string $language = two letter country code of the users language
	 */
	function __construct($session_id, $database_id, $language = null)
	{
		$this->session_id = $session_id;
		$this->database_id = $database_id;
		$this->language = strtoupper($language);
	}



	/**
	 * Notifies this user via poke about unread offline messages. But only, if the user hasn't been
	 * notified about these messages in recent time
	 *
	 * @param ts3admin &$ts3query = connection to the TS3 servers query
	 * @return array = {'success' => boolean, 'errors' => null|array, 'data' => null}
	 */
	function notify(ts3admin &$ts3query)
	{
		if($this->session_id !== false && count($this->messages) > 0) // only if user is currently online & has messages
		{
			if($this->notification_time + NOTIFICATION_POKE_TIMEOUT_DURATION < time())
			{
				$language = array_key_exists($this->language, LANGUAGES) ? LANGUAGES[$this->language] : LANGUAGES[DEFAULT_LANGUAGE];
				$message = (count($this->messages) == 1 ? $language[0] : $language[1]).' '.$language[2];

				$poke = $ts3query->clientPoke($this->session_id, str_replace('%n', count($this->messages), $message));
				if($poke['success'])
				{
					$this->notification_time = time();
				}

				return $poke;
			}
		}

		return array('success' => true, 'errors' => null, 'data' => null);
	}



	/**
	 * Fetches the IDs of all unread messages from the TS3 servers database, for every user that is
	 * currently online, and adds the IDs to the users respective client object
	 *
	 * @param mysqli &$db = connection to the TS3 servers database
	 * @param array &$clients = list of client objects, of all clients that are currently online
	 * @return void
	 *
	 * @throws Exception Failed to fetch offline messages from the TS3 servers database!
	 */
	static function fetch_unread_messages(mysqli &$db, array &$clients)
	{
		$sql = $db->query('select m.message_to_client_id as client_id, m.message_id from messages as m, servers as s where s.server_port = "'.TS3_SERVER_PORT.'" && s.server_id = m.server_id && m.message_flag_read = 0');
		if($sql)
		{
			while($row = $sql->fetch_object())
			{
				// in case the client isn't currently online
				if(!isset($clients[$row->client_id]))
				{
					$clients[$row->client_id] = new client(false, $row->client_id);
				}

				$clients[$row->client_id]->messages[] = $row->message_id;
			}

			return array('success' => true, 'errors' => null, 'data' => null);
		}

		trigger_error($db->error, E_USER_NOTICE);
		throw new Exception('Failed to fetch offline messages from the TS3 servers database!');
	}



	/**
	 * Exports the notification time & list of unread messages of all passed clients into an array
	 *
	 * @param array &$clients
	 * @return array = {'client_database_id' => {'notification_time', {'message_id', ...}}, ...}
	 */
	static function export(array &$clients)
	{
		$export = array();
		foreach($clients as $client)
		{
			if(count($client->messages) > 0)
			{
				$export += array($client->database_id => array($client->notification_time, $client->messages));
			}
		}

		return $export;
	}



	/**
	 * Imports data from the last execution. If a client hasn't received any new messages, his last
	 * notification time will be imported. If a client isn't currently online, all his data will be
	 * imported, so that his data will not be reset
	 *
	 * @param array &$clients = list of client objects, of all clients that are currently online
	 * @param array $memory = the exported data from the last execution
	 * @return void
	 */
	static function import(array &$clients, array $memory)
	{
		foreach($memory as $client_id => $data)
		{
			// in case the client isn't currently online
			if(!isset($clients[$client_id]))
			{
				$clients[$client_id] = new client(false, $client_id);
			}

			// only import last notification time if the user hasn't got any new messages
			if(count(array_diff($clients[$client_id]->messages, $data[1])) == 0)
			{
				$clients[$client_id]->notification_time = $data[0];
			}
		}
	}
}

?>
