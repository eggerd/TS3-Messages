2.1.1 - 23.02.2022
------------------
- Fixed some broken error triggers


2.1.0 - 29.07.2019
------------------
- At least PHP 7.0 is now required
- Updated configuration handler to version 2.4.0
- Added [Bugsnag](https://www.bugsnag.com/) integration for error tracking
- [ts3admin.class](https://github.com/par0noid/ts3admin.class) is now integrated using Composer


2.0.2 - 24.03.2019
------------------
- Fixed "*nickname is already in use*" error on servers running version 3.7.0, which was caused by the new naming convention for query users 
- Refactored error handling to use uncaught exceptions for all critical actions; and warnings/notices for everything else
- Updated [ts3admin.class](https://github.com/par0noid/ts3admin.class) to version 1.0.2.5
- Fixed some spelling errors


2.0.1 - 16.11.2018
------------------
- Fixed a bug that caused the script to repeatedly fail, if a user received a new message while he was offline


2.0.0 - 27.09.2018
------------------
- **Issue** #3: Nickname that will be used when connecting to the Teamspeak is now configurable
- **Issue** #8: A users language is now determined via TS3 query, instead of his profile settings on the community website
- **Issue** #9: The DocBlock format is now used for code documentation
- Completely rewrote code to be object oriented 
- Localization for more than just German and English is now supported
- Added readme with description of the project, requirements, installation instructions etc.
- This software is now licensed freely under the MIT license


1.1.1 - 26.07.2017
------------------
- **Issue** #7: Rewrote loop that determines if notifications should be sent, so that it checks the group only as last resort
- **Issue** #5: Updated configuration handler to version 2.2.1
- **Issue** #5: Confidential settings have been exported to a untracked `secrets.php` file and will no longer be inserted during the build
- **Issue** #2: Updated ts3admin.class to version 1.0.2.1


1.1.0 - 05.07.2017
------------------
- **Issue** #1: Translated project into English
- The changelog is now formatted in Markdown
- Replaced various white spaces with tabulators
- Moved version number from comments into configuration file


1.0.1 - 10.03.2017
------------------
- `messages.dat` has been moved to `/data/` and has also been renamed into `evaluated.json`
- If `/data/` does not exist, `setData()` now tries to create the directory
- ´setData()` is now called by `readData()`, if `evaluated.json` isn't readable


1.0.0 - 03.09.2016
------------------
- Initial release
