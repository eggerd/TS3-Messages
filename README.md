# TS3-Messages

[![version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F3272258%2Frepository%2Ftags&query=%24%5B0%5D.name&colorB=blue)](https://gitlab.com/eggerd/TS3-Messages/-/releases)
[![pipeline status](https://gitlab.com/eggerd/TS3-Messages/badges/master/pipeline.svg)](https://gitlab.com/eggerd/TS3-Messages/-/pipelines) 
[![quality gate](https://sonarcloud.io/api/project_badges/measure?project=ts3-messages&metric=alert_status)](https://sonarcloud.io/dashboard?id=ts3-messages) 
[![maintainability](https://sonarcloud.io/api/project_badges/measure?project=ts3-messages&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=ts3-messages)

Teamspeak 3 has a feature that allows users to leave offline messages for other users. Sadly, receivers of such messages never seem to notice them, because Teamspeak only shows a small insignificant symbol at the bottom of the window to indicate unread offline messages.  

This script simply notifies users via poke about any unread offline messages they got. The script will wait a configurable time before reminding a user again about his unread offline messages, unless he receives a new one in the meantime.

The language used for the notifications is based on the users location, which is provided by the TS3 query. Therefore, the notification messages can be easily localized into different languages - currently, only German and English are included.

In short:
- Notify users via poke about unread offline messages they have
- Configurable wait time before reminding a user again about his unread offline messages
- Supports easy localization of notification messages (currently only German & English are included)

## Requirements

- PHP 7.0 or higher
- Teamspeak Server 3.2.0 or higher
- Read access to the database of the Teamspeak 3 server, for at least [those tables](https://gitlab.com/eggerd/TS3-Messages/-/wikis/home#teamspeak-3-database)
- Access to the Teamspeak 3 query over Telnet, with a query account that has at least [these permissions](https://gitlab.com/eggerd/TS3-Messages/-/wikis/home#teamspeak-3-query)
- Ability to schedule cronjobs
- [Composer](https://getcomposer.org/), to install dependencies

## Installation

1. Download the [latest version](https://gitlab.com/eggerd/TS3-Messages/-/releases)
1. Run `composer install` in the root directory of the project
1. Edit the [configuration](https://gitlab.com/eggerd/TS3-Messages/-/wikis/home#available-settings) files in `configs/` according to your needs
1. Upload all files to your server
1. [Set up a cronjob](https://gitlab.com/eggerd/TS3-Messages/-/wikis/home#cronjob) that executes the `index.php`

> The script accesses the Teamspeak 3 query over Telnet. Therefore, traffic between the script and the TS3 query is not encrypted! Due to this, it is recommended that the script is executed on the same machine as the TS3 server itself.

## Documentation

A documentation with more details can be found in the [wiki](https://gitlab.com/eggerd/TS3-Messages/-/wikis) of the repository.

## Licensing

This software is available freely under the MIT License.  
Copyright (c) 2018 Dustin Eckhardt